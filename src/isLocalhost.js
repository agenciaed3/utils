const isLocalhost = ~window.location.host.indexOf('localhost') ? true : false;

export default isLocalhost;