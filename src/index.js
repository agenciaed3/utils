export { default as addLoading } from './addLoading';
export { default as removeLoading } from './removeLoading';
export { default as isMobile } from './isMobile';
export { default as slugify } from './slugify';
export { default as getUrlParameter } from './getUrlParameter';
export { default as removeUrlParameter } from './removeUrlParameter';
export { default as debounce } from './debounce';
export { default as waitForElement } from './waitForElement';