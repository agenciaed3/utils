const head = (arr) => (arr != null && arr.length ? arr[0] : false);
export default head;