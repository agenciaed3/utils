const removeLoading = (target) => target.classList.remove('is-loading');
export default removeLoading;