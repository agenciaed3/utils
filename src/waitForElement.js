const waitForElement = (target, callback, interval = 500) => {
    var tries = 0;
    var checkExist = setInterval(function () {
      if ($(target).length) {
        clearInterval(checkExist);
        callback && callback()
      }
      else {
        tries++
        if (tries > 50) {
          clearInterval(checkExist);
        }
      }
    }, interval);
  }

export default waitForElement;
  