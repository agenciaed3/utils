/**
 * @module utils
 */
const trimAmpersands = (str) => str
  .split('&')
  .filter(Boolean)
  .join('&');

export default trimAmpersands;