/**
 * @module utils
 */
const removeQuestionMarks = (str) => str.replace(/\?/g, '');

export default removeQuestionMarks;
