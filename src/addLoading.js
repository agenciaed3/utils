const addLoading = (target) => target.classList.add('is-loading');
export default addLoading;