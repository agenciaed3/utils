const trimSlashes = (str) => str.replace(/^\/+|\/+$/g, '');

export default trimSlashes;
