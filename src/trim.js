const trim = (str) =>
  typeof str === 'string' ? str.replace(/^\s+|\s+$/g, '') : '';
export default trim;
