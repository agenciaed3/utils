"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.utilify = void 0;

var _utilifyJs = _interopRequireDefault(require("utilify-js"));

var utilify = new _utilifyJs.default();
exports.utilify = utilify;