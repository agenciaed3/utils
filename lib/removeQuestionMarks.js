"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/**
 * @module utils
 */
var removeQuestionMarks = function removeQuestionMarks(str) {
  return str.replace(/\?/g, '');
};

var _default = removeQuestionMarks;
exports.default = _default;