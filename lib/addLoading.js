"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var addLoading = function addLoading(target) {
  return target.classList.add('is-loading');
};

var _default = addLoading;
exports.default = _default;