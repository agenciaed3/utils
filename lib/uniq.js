"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var uniq = function uniq(arr) {
  return arr.filter(function (value, index, self) {
    return self.indexOf(value) === index;
  });
};

var _default = uniq;
exports.default = _default;