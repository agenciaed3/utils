"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _trim = _interopRequireDefault(require("./trim"));

var _trimAmpersands = _interopRequireDefault(require("./trimAmpersands"));

var _removeQuestionMarks = _interopRequireDefault(require("./removeQuestionMarks"));

/**
 * @module utils
 */
var clearQuery = function clearQuery(query) {
  return (0, _removeQuestionMarks.default)((0, _trimAmpersands.default)((0, _trim.default)(query)));
};

var _default = clearQuery;
exports.default = _default;