"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var removeLoading = function removeLoading(target) {
  return target.classList.remove('is-loading');
};

var _default = removeLoading;
exports.default = _default;