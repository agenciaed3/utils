"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/**
 * Check if a string is a valid mail.
 *
 * @category Validate
 * @param {string} email - The string to check
 *
 * @module utils
 *
 * @return {boolean}
 */
var isEmail = function isEmail(email) {
  var regex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
  return regex.test(email);
};

var _default = isEmail;
exports.default = _default;