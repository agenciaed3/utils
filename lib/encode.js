"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var stringifyPrimitive = function stringifyPrimitive(v) {
  switch ((0, _typeof2.default)(v)) {
    case 'string':
      return v;

    case 'boolean':
      return v ? 'true' : 'false';

    case 'number':
      return isFinite(v) ? v : '';

    default:
      return '';
  }
};

var stringify = function stringify(obj) {
  var sep = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '&';
  var eq = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '=';
  var name = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '';

  if ((0, _typeof2.default)(obj) === 'object') {
    return Object.keys(obj).map(function (k) {
      var ks = encodeURIComponent(stringifyPrimitive(k)) + eq;

      if (Array.isArray(obj[k])) {
        return obj[k].map(function (v) {
          return ks + encodeURIComponent(stringifyPrimitive(v));
        }).join(sep);
      }

      return ks + encodeURIComponent(stringifyPrimitive(obj[k]));
    }).filter(Boolean).join(sep);
  }

  if (!name) return '';
  return encodeURIComponent(stringifyPrimitive(name)) + eq + encodeURIComponent(stringifyPrimitive(obj));
};

var _default = stringify;
exports.default = _default;