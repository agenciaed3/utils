"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var slugify = function slugify(str) {
  var isString = function isString(str) {
    return typeof str === 'string';
  };

  if (!isString(str)) return false;
  var a = 'àáäâãåèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;';
  var b = 'aaaaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------';
  var p = new RegExp(a.split('').join('|'), 'g');
  return str.toLowerCase().replace(/\s+/g, '-').replace(p, function (c) {
    return b.charAt(a.indexOf(c));
  }).replace(/&/g, '-e-').replace(/[^\w\-]+/g, '').replace(/\-\-+/g, '-').replace(/^-+/, '').replace(/-+$/, '');
};

var _default = slugify;
exports.default = _default;