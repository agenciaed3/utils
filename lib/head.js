"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var head = function head(arr) {
  return arr != null && arr.length ? arr[0] : false;
};

var _default = head;
exports.default = _default;