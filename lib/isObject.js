"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

/**
 * @module utils
 */
var isObject = function isObject(value) {
  return value !== null && (0, _typeof2.default)(value) === 'object';
};

var _default = isObject;
exports.default = _default;