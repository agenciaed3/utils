"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var waitForElement = function waitForElement(target, callback) {
  var interval = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 500;
  var tries = 0;
  var checkExist = setInterval(function () {
    if ($(target).length) {
      clearInterval(checkExist);
      callback && callback();
    } else {
      tries++;

      if (tries > 50) {
        clearInterval(checkExist);
      }
    }
  }, interval);
};

var _default = waitForElement;
exports.default = _default;