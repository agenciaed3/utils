"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var trimSlashes = function trimSlashes(str) {
  return str.replace(/^\/+|\/+$/g, '');
};

var _default = trimSlashes;
exports.default = _default;