"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var trim = function trim(str) {
  return typeof str === 'string' ? str.replace(/^\s+|\s+$/g, '') : '';
};

var _default = trim;
exports.default = _default;