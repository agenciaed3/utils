"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/**
 * @module utils
 */
var trimAmpersands = function trimAmpersands(str) {
  return str.split('&').filter(Boolean).join('&');
};

var _default = trimAmpersands;
exports.default = _default;